#Feature: E2E testing driver details page using cucumber

#Scenario: There is atleast one driver in the driver details page.
#When I open the url "http://google.com"
#Then i see drivers listed
Feature:
    In order to keep my product stable
    As a developer or product manager
    I want to make sure that everything works as expected

Scenario: Check title of website after search
    Given I open the url "https://tcs-sat.netlify.com"
    Then I wait on element "#root" for 10000ms to exist
    Then I expect that the title is "Auto Insurance"
    When I click on the button "#button2"
    Then I expect that the url is "https://tcs-sat.netlify.com/getstarted"
    When I add "75605" to the inputfield "#zipcode"
    When I add "Longview" to the inputfield "#city"
    When I add "TEXAS" to the inputfield "#state" 